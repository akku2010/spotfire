import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcReportPage } from './ac-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AcReportPage,
  ],
  imports: [
    IonicPageModule.forChild(AcReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class AcReportPageModule {}
