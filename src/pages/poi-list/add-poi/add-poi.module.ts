import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPoiPage } from './add-poi';
import { TranslateModule } from '@ngx-translate/core';
// import { NativeGeocoder } from '@ionic-native/native-geocoder';
@NgModule({
  declarations: [
    AddPoiPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPoiPage),
    TranslateModule.forChild()
  ],
  providers: [],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AddPoiPageModule { }
