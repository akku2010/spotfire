import { Component, OnInit } from "@angular/core";
import { NavParams, AlertController, IonicPage, Platform } from "ionic-angular";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { LatLngBounds, GoogleMaps, Marker, LatLng, Spherical } from "@ionic-native/google-maps";

@IonicPage()
@Component({
  selector: 'page-trip-review',
  templateUrl: 'trip-review.html'
})
export class TripReviewPage implements OnInit {
  tripData: any;
  Device_Id: any;
  points: any;
  mapData: any[];
  dataArrayCoords: any[];
  playbackData: any[];
  speed1: any;
  playing: boolean;
  coordreplaydata: any;
  speed: number;
  speedMarker: any;
  updatetimedate: any;
  target: number = 0;
  allData: any = {};
  startPos: any[];
  islogin: any;

  constructor(
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public plt: Platform) {
    debugger
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("pramas=> " + JSON.parse(JSON.stringify(navParams.get("params"))));
    this.tripData = navParams.get("params");
    this.Device_Id = this.tripData.Device_ID;
  }

  ngOnInit() {
    // localStorage.removeItem("tripTarget");
    // this.triphistory();
  }

  ionViewDidEnter() {
    this.getDefaultUserSettings();
    localStorage.removeItem("tripTarget");
    this.triphistory();
  }

  measurementUnit: string = 'MKS';
  getDefaultUserSettings() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }

  onShare() {

    let tempalert = this.alertCtrl.create({
      title: 'Share Trip',
      message: 'Enter your Mobile Number so that we can share you a trip',
      inputs: [
        {
          name: 'mobno',
          placeholder: 'Mobile Number',
          type: 'number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Share',
          handler: data => {
            var sNumber = data.mobno.toString();
            if (sNumber.length == 10) {
              console.log(data.mobno)
            } else {
              alert("Plaese provide valid mobile number")
            }
          }
        }
      ]
    });
    tempalert.present();
  }

  triphistory() {
    debugger
    let that = this;
    that.apiCall.startLoading().present();
    that.apiCall.tripReviewCall(that.Device_Id, that.tripData.start_time, that.tripData.end_time)
      .subscribe(data => {
        that.apiCall.stopLoading();

        that.points = data;
        that.mapData = [];
        that.mapData = that.points.map(function (d) {
          return { lat: d.lat, lng: d.lng };
        });
        // that.points.reverse();
        let latLngBounds = new LatLngBounds(that.mapData);
        that.dataArrayCoords = [];

        for (var i = that.points.length - 1; i > 0; i--) {

          if (that.points[i].lat && that.points[i].lng) {
            var arr = [];
            var startdatetime = new Date(that.points[i].insertionTime);
            arr.push(that.points[i].lat);
            arr.push(that.points[i].lng);
            arr.push({ "time": startdatetime.toLocaleString() });
            arr.push({ "speed": that.points[i].speed });
            that.dataArrayCoords.push(arr);
          }
        }

        setInterval(() => {
          that.dataArrayCoords;
        }, 0);
        // console.log(that.dataArrayCoords);

        that.playbackData = [];

        for (var j = 0; j < that.points.length; j++) {
          var startdatetime1 = new Date(that.points[j].insertionTime);

          that.playbackData.push({ "lat": that.points[j].latDecimal, "lng": that.points[j].longDecimal, 'speed': that.points[j].speed + "Km/hr", 'isettiontime': startdatetime1.toLocaleString() });

          that.speed1 = that.playbackData[j].speed;

          setInterval(() => {
            that.speed1;
          }, 0);
        }

        if (that.allData.map != undefined) {
          that.allData.map.remove();
        }
        that.allData.map = GoogleMaps.create('trip_map_canvas');

        that.allData.map.moveCamera({
          'target': latLngBounds
        });


        that.allData.map.addMarker({
          title: 'S',
          position: that.mapData[0],
          icon: 'green',
          styles: {
            'text-align': 'center',
            'font-style': 'italic',
            'font-weight': 'bold',
            'color': 'red'
          },
          // icon: 'http://www.googlemapsmarkers.com/v1/FF0000/'
        }).then((marker: Marker) => {
          marker.showInfoWindow();

          that.allData.map.addMarker({
            title: 'D',
            position: that.mapData[that.mapData.length - 1],
            icon: 'red',
            styles: {
              'text-align': 'center',
              'font-style': 'italic',
              'font-weight': 'bold',
              'color': 'green'
            },
            // icon: 'http://www.googlemapsmarkers.com/v1/009900/'
          }).then((marker: Marker) => {
            marker.showInfoWindow();
            // animateMarker(marker, dataArrayCoords, speed, trackerType)
          });
        });

        // console.log("latlang.............", that.mapData)
        if (that.mapData.length > 1) {
          that.allData.map.addPolyline({
            points: that.mapData,
            color: 'blue',
            width: 4,
            geodesic: true
          })
        }
      },
        err => {
          console.log(err);
          that.apiCall.stopLoading();
        });
  };

  Playback() {
    let that = this;
    if (localStorage.getItem("tripTarget") != null) {
      that.target = JSON.parse(localStorage.getItem("tripTarget"));
    }
    that.playing = !that.playing; // This would alternate the state each time
    var coord = that.dataArrayCoords[that.target];
    that.coordreplaydata = coord;
    var lat = coord[0];
    var lng = coord[1];

    that.startPos = [lat, lng];
    that.speed = 200; // km/h
    if (that.playing) {
      console.log("if initialize=> ", that.playing)

      that.allData.map.setCameraTarget({ lat: lat, lng: lng });
      if (that.allData.mark == undefined) {
        var markicon;
        if (this.plt.is('android')) {
          markicon = './assets/imgs/vehicles/runningcar.png';
        } else if (this.plt.is('ios')) {
          markicon = 'www/assets/imgs/vehicles/runningcar.png';
        }

        that.allData.map.addMarker({
          icon: {
            url: markicon,
            size: {
              width: 20,
              height: 40
            }
          },
          styles: {
            'text-align': 'center',
            'font-style': 'italic',
            'font-weight': 'bold',
            'color': 'green'
          },
          position: new LatLng(that.startPos[0], that.startPos[1]),
        }).then((marker: Marker) => {
          console.log("we reached here")
          that.allData.mark = marker;
          that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
        });
      } else {
        that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
        that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
      }

    } else {
      that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
    }
  }

  liveTrack(map, mark, coords, target, startPos, speed, delay,) {
    let that = this;
    var target = target;
    if (!startPos.length)
      coords.push([startPos[0], startPos[1]]);

    function _gotoPoint() {
      if (target > coords.length)
        return;
      console.log("then here")
      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;
      console.log("after here")
      var step = (speed * 1000 * delay) / 3600000;
      if (coords[target] == undefined)
        return;
      var dest = new LatLng(coords[target][0], coords[target][1]);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); //in meters
      var numStep = distance / step;
      var i = 0;

      var deltaLat = (coords[target][0] - lat) / numStep;
      var deltaLng = (coords[target][1] - lng) / numStep;

      function changeMarker(mark, deg) {
        console.log("check marker: ", mark)
        if (Number.isNaN(parseInt(deg))) {
          console.log("check degree: " + parseInt(deg))
        } else {
          console.log("check not: " + parseInt(deg))
          if (mark) {
            mark.setRotation(deg);
          }
        }
      }

      function _moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));

        if (i < distance) {
          if ((head !== 0) || (head !== NaN)) {
            changeMarker(mark, head);
          }
          mark.setPosition(new LatLng(lat, lng));
          setTimeout(_moveMarker, delay);
        } else {
          if ((head !== 0) || (head !== NaN)) {
            changeMarker(mark, head);
          }
          mark.setPosition(dest);
          target++;
          setTimeout(_gotoPoint, delay);
        }
      }
      a++
      if (a > coords.length) {

      } else {
        that.speedMarker = coords[target][3].speed;
        that.updatetimedate = coords[target][2].time;

        if (that.playing) {
          _moveMarker();
          target = target;
          localStorage.setItem("tripTarget", target);

        } else { }
      }

    }
    var a = 0;
    _gotoPoint();
  }

  inter(fastforwad) {
    let that = this;
    if (fastforwad == 'fast') {
      that.speed = 2 * that.speed;
    }
    else if (fastforwad == 'slow') {
      if (that.speed > 50) {
        that.speed = that.speed / 2;
      }
      else {
      }
    }
    else {
      that.speed = 200;
    }
  }
}
